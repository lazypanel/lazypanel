# [lazyPanel](http://lazypanel.com) - A powerful server management suite with all the goodness of free

lazyPanel is a clean, easy to use, powerful management suite for all your
server management needs. Freeing you from the usual headaches, managing daemons,
monitoring servers, installing software, updating and optimising. Running your
servers should be a hassle free experience that you enjoy, not something that is
painful. Enjoy the fresh air.

## Feature Overview

## Contributing to lazyPanel

Contributions are encouraged and welcome; 

## License

lazyPanel is open-sourced software licensed under the MIT License.

## Demo

Want to try out a demo before you install? Head over to our really easy to use [demo](http://demo.lazypanel.com)
and give us a try!

## Statistics

We're tracking all our commits, over on Ohloh. Check us out by [clicking here](https://www.ohloh.net/p/lazypanel)
